-- PARAGRAPHS
INSERT INTO paragraph (id, content)
VALUES (1,
        'I\'m a young developer with lots of energy, ready to learn every single thing that might be useful. I gather knowledge rather quickly. In spare time I experiment with Docker and AWS.');

INSERT INTO paragraph (id, content)
VALUES (2,
        'After hours I find peace in jogging. I hate trains, but love driving cars. The best time off is the one spent with a pack of friends on a road trip.');


-- GENERAL
INSERT INTO info (`id`, `key`, value, link)
VALUES (1, 'Age', '23', NULL);

INSERT INTO info (`id`, `key`, value, link)
VALUES (2, 'Location', 'Gdańsk, Poland', NULL);

INSERT INTO info (`id`, `key`, value, link)
VALUES (3, 'Phone', '+48 604436130', 'tel:+48604436130');

INSERT INTO info (`id`, `key`, value, link)
VALUES (4, 'E-Mail', 'kontakt@mentelm.pl', 'mailto:kontakt@mentelm.pl');

INSERT INTO info (`id`, `key`, value, link)
VALUES (5, 'linkedIn', 'mentelm', 'linkedin.com/in/mentelm');

-- SKILLS - Skills
INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('Java', 80, 'Skills', NULL);

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('Spring', 70, 'Skills', NULL);

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('Angular', 60, 'Skills', NULL);

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('JavaScript', 60, 'Skills', NULL);

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('HTML', 55, 'Skills', NULL);

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('Hibernate', 50, 'Skills', NULL);

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('CSS', 40, 'Skills', NULL);

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('SQL', 20, 'Skills', NULL);

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('Docker', 15, 'Skills', NULL);

-- SKILLS - Languages
INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('Polish', 95, 'Languages', 'Native');

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('English', 85, 'Languages', 'C1/C2');

INSERT INTO skill (`name`, `level`, `category_name`, `info`)
VALUES ('Russian', 20, 'Languages', NULL);

-- TIMELINES - Experience
INSERT INTO timeline_event (id, company, details, `from`, name, timeline_name, `to`)
VALUES (1,
        'Nordea',
        'Co-responsible for growth and modernization of mortgage system for Nordic countries. As a member of System Team I am involved in creating guidelines for other teams, decision-making processes, planning and introducing improvements (e.g. CI/CD pipelines, Dockerization), and troubleshooting problems from production.',
        '12.2017',
        'IT Developer',
        'Experience',
        'today');

-- TIMELINES - Certificates
INSERT INTO timeline_event (id, company, name, `from`, `to`, timeline_name)
VALUES (2,
        'Oracle',
        'JAVA SE 8 Programmer I',
        '10.2018',
        NULL,
        'Certificates');

INSERT INTO timeline_event (id, company, name, `from`, `to`, timeline_name)
VALUES (3,
        'Scaled Agile Inc.',
        'Certified SAFe 4 Practitioner',
        '03.2019',
        '01.2020',
        'Certificates');
