create table `info`
(
    `id`    bigint       not null primary key,
    `key`   varchar(255) not null,
    `link`  varchar(255) null,
    `value` varchar(255) not null
);

create table `paragraph`
(
    `id`      bigint       not null primary key,
    `content` varchar(255) not null
);

create table `skill`
(
    `name`          varchar(255) not null primary key,
    `category_name` varchar(255) not null,
    `info`          varchar(255) null,
    `level`         int          not null
);

create table `timeline_event`
(
    `id`            bigint        not null primary key,
    `company`       varchar(255)  null,
    `details`       varchar(1023) null,
    `from`          varchar(255)  not null,
    `name`          varchar(255)  not null,
    `timeline_name` varchar(255)  not null,
    `to`            varchar(255)  null
);

