-- Courses
INSERT INTO timeline_event (id, company, `from`, `to`, name, timeline_name) VALUES
(4, 'Software Development Academy', '07.2017', '11.2017', 'IT na Bank', 'Courses');

INSERT INTO timeline_event (id, company, `from`, name, timeline_name) VALUES
(5, 'BNS IT', '01.2018', 'Java Developers\' Bootcamp', 'Courses');

INSERT INTO timeline_event (id, company, `from`, name, timeline_name) VALUES
(6, 'BNS IT', '05.2019', 'Spring Framework in practice', 'Courses');

INSERT INTO timeline_event (id, company, `from`, name, timeline_name) VALUES
(7, 'Piotr Bucki', '2019', 'Good communication: good presentation', 'Courses');
