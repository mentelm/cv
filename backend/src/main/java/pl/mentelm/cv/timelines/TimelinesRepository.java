package pl.mentelm.cv.timelines;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimelinesRepository extends JpaRepository<TimelineEvent, Long> {
    List<TimelineEvent> getByTimelineName(String timelineName);

    @Query("SELECT DISTINCT timelineName FROM TimelineEvent")
    List<String> getTimelineNames();
}
