package pl.mentelm.cv.timelines;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TimelineEvent {
    @Id
    @GeneratedValue
    public Long id;

    public String from;
    public String to;
    public String name;
    public String company;
    public String details;

    public String timelineName;
}
