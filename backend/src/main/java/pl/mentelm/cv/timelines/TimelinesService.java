package pl.mentelm.cv.timelines;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TimelinesService {
    private final TimelinesRepository repository;

    public List<String> getTimelineNames() {
        return repository.getTimelineNames();
    }

    public List<TimelineEvent> getEventsByTimeline(String timelineName) {
        return repository.getByTimelineName(timelineName);
    }

    public List<TimelineEvent> getAll() {
        return repository.findAll();
    }
}
