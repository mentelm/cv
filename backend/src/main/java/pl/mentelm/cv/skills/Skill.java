package pl.mentelm.cv.skills;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Skill {
    @Id
    public String name;
    public int level;
    public String info;
    public String categoryName;
}
