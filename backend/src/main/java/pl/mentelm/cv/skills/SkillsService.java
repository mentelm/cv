package pl.mentelm.cv.skills;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SkillsService {
    private final SkillsRepository repository;

    public List<String> getCategoryNames() {
        return repository.getCategoryNames();
    }

    public List<Skill> getSkillsByCategory(String categoryName) {
        return repository.getByCategoryName(categoryName);
    }

    public List<Skill> getAll() {
        return repository.findAll();
    }
}
