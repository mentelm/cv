package pl.mentelm.cv.skills;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SkillsRepository extends JpaRepository<Skill, String> {
    List<Skill> getByCategoryName(String categoryName);

    @Query("SELECT DISTINCT categoryName FROM Skill")
    List<String> getCategoryNames();
}
