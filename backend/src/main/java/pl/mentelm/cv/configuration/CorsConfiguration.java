package pl.mentelm.cv.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
@ConditionalOnProperty("corsOrigin")
public class CorsConfiguration implements WebMvcConfigurer {
    private final String corsOrigin;

    public CorsConfiguration(@Value("corsOrigin") String corsOrigin) {
        this.corsOrigin = corsOrigin;
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        log.info("Setting allowed origin.");
        registry.addMapping("/**")
                .allowedOrigins(corsOrigin);
    }
}
