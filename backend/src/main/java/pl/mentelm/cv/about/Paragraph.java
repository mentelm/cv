package pl.mentelm.cv.about;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Paragraph {
    @Id
    public Long id;
    public String content;
}
