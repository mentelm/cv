package pl.mentelm.cv.about;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParagraphsRepository extends JpaRepository<Paragraph, Long> {
}
