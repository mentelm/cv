package pl.mentelm.cv.about;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ParagraphsService {
    private final ParagraphsRepository paragraphsRepository;

    public List<Paragraph> getParagraphs() {
        return paragraphsRepository.findAll();
    }
}
