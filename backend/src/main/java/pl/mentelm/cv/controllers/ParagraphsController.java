package pl.mentelm.cv.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mentelm.cv.about.ParagraphsService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/paragraphs", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ParagraphsController {
    private final ParagraphsService service;

    @GetMapping
    public List<String> getAllParagraphs() {
        return service.getParagraphs().stream()
                .map(paragraph -> paragraph.content)
                .collect(Collectors.toList());
    }
}
