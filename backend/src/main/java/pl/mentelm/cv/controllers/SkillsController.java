package pl.mentelm.cv.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mentelm.cv.skills.Skill;
import pl.mentelm.cv.skills.SkillsService;

import java.util.List;

@RestController
@RequestMapping(path = "/skills", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class SkillsController {
    private final SkillsService service;

    @GetMapping(path = "/categories")
    public List<String> getSkillCategoryNames() {
        return service.getCategoryNames();
    }

    @GetMapping(path = "/categories/{name}")
    public List<Skill> getSkillsByCategory(@PathVariable("name") String categoryName) {
        return service.getSkillsByCategory(categoryName);
    }

    @GetMapping
    public List<Skill> getAllSkills() {
        return service.getAll();
    }
}
