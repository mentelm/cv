package pl.mentelm.cv.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mentelm.cv.timelines.TimelineEvent;
import pl.mentelm.cv.timelines.TimelinesService;

import java.util.List;

@RestController
@RequestMapping(path = "/events")
@RequiredArgsConstructor
public class TimelinesController {
    private final TimelinesService service;

    @GetMapping(path = "/timelines")
    public List<String> getTimelineCategoryNames() {
        return service.getTimelineNames();
    }

    @GetMapping(path = "/timelines/{name}")
    public List<TimelineEvent> getEventsByTimeline(@PathVariable("name") String categoryName) {
        return service.getEventsByTimeline(categoryName);
    }

    @GetMapping
    public List<TimelineEvent> getAllTimelines() {
        return service.getAll();
    }
}
