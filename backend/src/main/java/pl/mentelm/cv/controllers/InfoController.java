package pl.mentelm.cv.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mentelm.cv.info.Info;
import pl.mentelm.cv.info.InfoService;

import java.util.List;

@RestController
@RequestMapping(path = "/general", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class InfoController {
    private final InfoService service;

    @GetMapping
    public List<Info> getAllInformation() {
        return service.getAll();
    }
}
