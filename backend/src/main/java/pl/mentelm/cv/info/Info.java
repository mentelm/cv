package pl.mentelm.cv.info;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Info {
    @Id
    public long id;
    public String key;
    public String value;
    public String link;
}
