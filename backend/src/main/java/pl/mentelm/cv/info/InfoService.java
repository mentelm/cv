package pl.mentelm.cv.info;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class InfoService {
    private final InfoRepository repository;

    public List<Info> getAll() {
        return repository.findAll();
    }
}
