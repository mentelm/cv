import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {SkillsComponent} from './skills/skills.component';
import {TimelinesComponent} from './timelines/timelines.component';
import {AboutComponent} from './about/about.component';
import {GeneralComponent} from './general/general.component';
import {HeaderComponent} from './header/header.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {Configuration, ConfigurationParameters, CvClientApiModule} from 'CvClient';
import {environment} from '../environments/environment';

export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
    basePath: environment.backendUrl
  };
  return new Configuration(params);
}

@NgModule({
  declarations: [
    AppComponent,
    SkillsComponent,
    TimelinesComponent,
    AboutComponent,
    HeaderComponent,
    GeneralComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    CvClientApiModule.forRoot(apiConfigFactory),
    NgbModule,
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: 'pl',
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http),
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
