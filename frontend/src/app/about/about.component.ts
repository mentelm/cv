import {Component, OnInit} from '@angular/core';
import {DefaultCvClient} from 'CvClient';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit {
  text: Array<string> = [];

  constructor(private client: DefaultCvClient) {
  }

  ngOnInit(): void {
    this.client.getAllParagraphs('body')
      .subscribe(paragraphs => this.text = paragraphs);
  }
}
