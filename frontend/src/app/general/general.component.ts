import {Component, OnInit} from '@angular/core';
import {DefaultCvClient, Info} from 'CvClient';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html'
})
export class GeneralComponent implements OnInit {
  infos: Array<Info> = [];

  constructor(private client: DefaultCvClient) {
  }

  ngOnInit(): void {
    this.client.getAllInformation('body')
      .subscribe(info => this.infos = info);
  }
}
