import {Component, OnInit} from '@angular/core';
import {DefaultCvClient, TimelineEvent} from 'CvClient';

@Component({
  selector: 'app-timelines',
  templateUrl: './timelines.component.html'
})
export class TimelinesComponent implements OnInit {
  timelines: Timeline[] = [];

  constructor(private client: DefaultCvClient) {
  }

  ngOnInit(): void {
    this.client.getAllTimelines('body')
      .subscribe(timelineEvents => this.timelines = TimelineEventAPIMapper.toTimelines(timelineEvents));
  }
}

class TimelineEventAPIMapper {
  public static toTimelines(sourceApis: TimelineEvent[]): Array<Timeline> {
    const timelines = new Map<string, Timeline>();

    for (const api of sourceApis) {
      let timeline: Timeline;

      if (!timelines.has(api.timelineName)) {
        timeline = {name: api.timelineName, events: []};
        timeline.name = api.timelineName;
        timelines.set(api.timelineName, timeline);
      } else {
        timeline = timelines.get(api.timelineName);
      }

      timeline.events.push(api);
    }

    const result: Array<Timeline> = [];
    for (const timeline of timelines.values()) {
      result.push(timeline);
    }
    return result;
  }
}

export interface Timeline {
  name: string;
  events: Array<TimelineEvent>;
}
