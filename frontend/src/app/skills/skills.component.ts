import {Component, OnInit} from '@angular/core';
import {DefaultCvClient, Skill} from 'CvClient';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html'
})
export class SkillsComponent implements OnInit {
  skills: SkillCategory[] = [];

  constructor(private client: DefaultCvClient) {
  }

  ngOnInit(): void {
    this.client.getAllSkills('body')
      .subscribe(skills => this.skills = this.toCategories(skills));
  }

  private toCategories(skills: Array<Skill>): SkillCategory[] {
    const skillCategories = new Map<string, SkillCategory>();

    for (const skill of skills) {
      if (!skillCategories.has(skill.categoryName)) {
        skillCategories.set(skill.categoryName, {
          name: skill.categoryName,
          skills: []
        });
      }

      const skillCategory = skillCategories.get(skill.categoryName);
      skillCategory.skills.push(skill);
    }

    const categories: SkillCategory[] = [];
    for (const skill of skillCategories.values()) {
      categories.push(skill);
    }

    for (const category of categories) {
      category.skills.sort((a, b) => a.level === b.level ? 0 : (a.level > b.level ? -1 : 1));
    }

    return categories;
  }

  public getColor(percentage: number): string {
    if (percentage < 30) {
      return 'danger';
    } else if (percentage < 50) {
      return 'warning';
    } else if (percentage < 70) {
      return 'success';
    } else {
      return 'info';
    }
  }
}

interface SkillCategory {
  name: string;
  skills: Array<Skill>;
}
