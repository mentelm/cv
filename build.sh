#!/bin/bash

timestamp=$(date '+%y%m%d%H%M%S')
root=$(pwd)
version="1.0.0"
revision="$version-L$timestamp"

cd $root/backend || return 1
mvn -B clean compile -Drevision=$revision -Dchangelog=SNAPSHOT
cd $root/backend/target/generated-sources/openapi || return 1
yarn install
yarn run build
cd dist || return 1
yarn pack --filename=cv_client_$revision-SNAPSHOT.tgz
cd $root/frontend || return 1
yarn remove CvClient
yarn add $root/backend/target/generated-sources/openapi/dist/cv_client_$revision-SNAPSHOT.tgz
yarn install
yarn run build-prod
mkdir -p $root/backend/src/main/resources/public
cp -r $root/frontend/dist/cv/* $root/backend/src/main/resources/public/

cd $root/backend || return 1
mvn -B package -Dcodegen.skip -Drevision=$revision -Dchangelog=SNAPSHOT

cd $root || return 1
docker build backend -t mentelm/cv:$revision-SNAPSHOT -t mentelm/cv:latest
